<?php

// Register shortcodes
paf_shortcodes( array(
	'product_stock' => array(
		'title'      => __( 'Product Stock' ),
		'image'      => 'http://png.findicons.com/files/icons/753/gnome_desktop/32/gnome_package_x_generic.png',
		'parameters' => array(
			'id' => array(
				'title'   => 'Product',
				'type'    => 'select',
				'options' => 'posts',
				'args'    => array(
					'post_type'   => 'product',
					'post_status' => 'publish',
				),
			),
		),
	),
) );

/**
 * Return output for the shortcode [product_stock id="XXX"]
 * 
 * The function retunrs:
 * - An empty string if the product id is not specified o
 *   does not correspond to a product
 * - This if the original stock is specified:
 *     <span class="wcss"><span class="wcss_1">XXX</span>/<span class="wcss_2">YYY</span></span>
 * - This if the original stock is not specified:
 *     <span class="wcss"><span class="wcss_1">XXX</span></span>
 */
function product_stock_func( $atts = array(), $content = null ) {

	// Return empty string if no product
	if ( empty ( $atts[ 'id' ] ) )  return '';

	$product_id = $atts[ 'id' ];
	$product = wc_get_product( $product_id );

	// Return empty string if wrong product provided
	if ( ! $product ) return '';

	$stock = $product->get_stock_quantity();
	$original_stock = get_field( 'wcss_original_stock', $product_id );

	return '<span class="wcss">'
		. ( $original_stock
			? sprintf( '<span class="wcss_1">%s</span>/<span class="wcss_2">%s</span>', $stock, $original_stock )
			: sprintf( '<span class="wcss_1">%s</span>', $stock )
		)
		. '</span>'
	;
}

