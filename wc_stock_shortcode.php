<?php

/*
Plugin Name: WooCommerce Stock Shortcode
Plugin URI: http://www.kadimi.com/
Description: -
Version: 1.0.0
Author: Nabil Kadimi
Author URI: http://www.kadimi.com/
License: GPL2
*/

// Constants
define( 'WCSC_PATH', plugin_dir_path( __FILE__ ) );

// Use ACF Lite
include WCSC_PATH . 'inc/plugins/advanced-custom-fields/acf.php';
define( 'ACF_LITE', true );

// Use Skelet Framework
include WCSC_PATH . '/skelet/skelet.php';
skelet_dir( WCSC_PATH . '/data' );

/**
 * Initialize plugin
 *
 * - Register field group
 */
add_action( 'init', 'wcss_init_cb' );
function wcss_init_cb() {
	
	// Add GoToWebcast fields
	register_field_group( array(
		'id' => 'acf_wcss',
		'title' => __( 'Additional Stock Information' ),
		'fields' => array(
			array(
				'key' => 'field_wcss_original_stock',
				'label' => __( 'Original Stock' ),
				'name' => 'wcss_original_stock',
				'type' => 'number',
				'default_value' => '',
				'placeholder' => 'e.g. 300',
				'prepend' => '',
				'append' => '',
				'min' => '',
				'max' => '',
				'step' => '',
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'product',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array(
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array(),
		),
		'menu_order' => 0,
	) );
}
